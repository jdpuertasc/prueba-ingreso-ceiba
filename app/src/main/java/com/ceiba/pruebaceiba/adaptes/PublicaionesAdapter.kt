package com.ceiba.pruebaceiba.adaptes

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ceiba.pruebaceiba.R
import com.ceiba.pruebaceiba.data.Publicaciones
import com.ceiba.pruebaceiba.data.User

class PublicaionesAdapter  : RecyclerView.Adapter<PublicaionesAdapter.ViewHolderPublicaciones>()  {

    var publicaciones: MutableList<Publicaciones>  = ArrayList()
    lateinit var context: Context

    fun PublicaionesAdapter(superheros : MutableList<Publicaciones>, context: Context){
        this.publicaciones = superheros
        this.context = context
    }


    class ViewHolderPublicaciones( view : View) : RecyclerView.ViewHolder(view){

        private val title = view.findViewById<TextView>(R.id.title)
        private val body = view.findViewById<TextView>(R.id.body)

        fun bind( publicaciones : Publicaciones){
            title.text = publicaciones.title
            body.text = publicaciones.body
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderPublicaciones {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolderPublicaciones(layoutInflater.inflate(R.layout.cart_view_publicacion, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolderPublicaciones, position: Int) {
        val item = publicaciones[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int {
        return publicaciones.size
    }


}