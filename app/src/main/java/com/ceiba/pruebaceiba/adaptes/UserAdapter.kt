package com.ceiba.pruebaceiba.adaptes

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ceiba.pruebaceiba.R
import com.ceiba.pruebaceiba.data.Publicaciones
import com.ceiba.pruebaceiba.data.User
import com.ceiba.pruebaceiba.interfaces.OnClickPublicacion

class UserAdapter  : RecyclerView.Adapter<UserAdapter.ViewHolderUser>() {

    var users: MutableList<User>  = ArrayList()
    lateinit var context: Context
    lateinit var onClickPublicacion : OnClickPublicacion

    fun UserAdapter( users : MutableList<User>,  context: Context,  onClickPublicacion: OnClickPublicacion){
        this.users = users
        this.context = context
        this.onClickPublicacion = onClickPublicacion
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderUser {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolderUser(layoutInflater.inflate(R.layout.item_cart_user, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolderUser, position: Int) {
        val item = users[position]
        holder.bind(item, onClickPublicacion, position)
    }

    override fun getItemCount(): Int {
       return users.size
    }

    class ViewHolderUser (view : View  ) : RecyclerView.ViewHolder(view) {
        private val nameUser = view.findViewById<TextView>(R.id.name)
        private val phone = view.findViewById<TextView>(R.id.phone)
        private val email = view.findViewById<TextView>(R.id.email)
        private val btnVerPerfil = view.findViewById<Button>(R.id.btnVerPerfil)


        fun bind(user: User, onClickPublicacion: OnClickPublicacion, position: Int){
            nameUser.text = user.name
            phone.text = user.phone
            email.text = user.email
            btnVerPerfil.setOnClickListener {
                onClickPublicacion.onClickPublicacion(user.id.toInt(), position )
            }

        }


    }


}


