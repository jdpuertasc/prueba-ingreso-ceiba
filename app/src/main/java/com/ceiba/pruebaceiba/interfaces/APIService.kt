package com.ceiba.pruebaceiba.interfaces

import com.ceiba.pruebaceiba.data.Publicaciones
import com.ceiba.pruebaceiba.data.User
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface APIService {
        @GET("users")
        suspend fun getUsuarios() : Response<List<User>>

        @GET
        suspend fun getPost(@Url userId: String) : Response<List<Publicaciones>>
}