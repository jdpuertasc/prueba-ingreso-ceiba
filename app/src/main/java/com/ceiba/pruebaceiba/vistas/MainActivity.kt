package com.ceiba.pruebaceiba.vistas

import android.content.ContentValues
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.SearchView
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ceiba.pruebaceiba.R
import com.ceiba.pruebaceiba.adaptes.UserAdapter
import com.ceiba.pruebaceiba.data.User
import com.ceiba.pruebaceiba.dbsql.BaseDatosLoca
import com.ceiba.pruebaceiba.interfaces.OnClickPublicacion
import com.ceiba.pruebaceiba.mainModule.model.viewModel.MainViewModel
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity(),
    OnClickPublicacion {

    private var adapter: UserAdapter = UserAdapter()
    private var listaUsuarios = mutableListOf<User>()
    lateinit var mRecyclerView : RecyclerView
    private val progressDialog = CustomProgressDialog()
    private lateinit var mainViewModel : MainViewModel
    val admin = BaseDatosLoca(this, "usuarios", null, 1)
    lateinit var searchView: SearchView





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        buscador()
        initRecyclerView()
        parseJson()

    }

    private fun buscador() {
        searchView = findViewById(R.id.svUser)

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                listaUsuarios.clear()
                val admin = BaseDatosLoca(applicationContext, "usuarios", null, 1)
                val bd = admin.writableDatabase
                val fila = bd.rawQuery("select  id, name, userName, email, phone  from usuarios where name LIKE '%${query}%'", null)
                var i = 0;

                if (fila.moveToFirst()) {

                    val id =  fila.getString(0)
                    val name = fila.getString(1)
                    val userName = fila.getString(2)
                    val email = fila.getString(3)
                    val phone = fila.getString(4)

                    var user = User(id, name, userName, email, phone, "", null, null)

                    listaUsuarios.add(user)

                    i++
                }  else {
                    setUpData()
                }

                bd.close()

                adapter.notifyDataSetChanged()

                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                listaUsuarios.clear()
                setUpData()
                return false
            }


        })
    }

    override fun onStart() {
        super.onStart()
        listaUsuarios.clear()
        setUpData()
    }

    private fun setUpData(){
        lifecycleScope.launch {
            mainViewModel.getListaUsuarios()
        }
    }

    private fun initRecyclerView() {

        mRecyclerView = findViewById(R.id.rvUser) as RecyclerView
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(this);
        adapter.UserAdapter( listaUsuarios, this, this)
        mRecyclerView.adapter = adapter

    }


    fun parseJson() {

        progressDialog.show(this)
        mainViewModel = ViewModelProvider(this)[MainViewModel::class.java]
        mainViewModel.getResult().observe(this){ result ->
            setUpdateUi(result)
        }

    }

    private fun setUpdateUi (data: User
    ){
        listaUsuarios.add(data)

        val bd = admin.writableDatabase

        val registro = ContentValues()
        registro.put("id", data.id.toString())
        registro.put("name", data.name)
        registro.put("username", data.username)
        registro.put("email", data.email)
        registro.put("phone", data.phone)
        bd.insert("usuarios", null, registro)
        bd.close()


        progressDialog.dialog.dismiss()
        adapter.notifyDataSetChanged()
    }


    override fun onClickPublicacion(id: Int, position : Int) {

        intent = Intent(applicationContext, PublicacionesActivity::class.java)
        intent.putExtra("id", listaUsuarios[position].id)
        intent.putExtra("name", listaUsuarios[position].name)
        intent.putExtra("phone", listaUsuarios[position].phone)
        intent.putExtra("email", listaUsuarios[position].email)
        startActivity(intent)

    }


}