package com.ceiba.pruebaceiba.vistas

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ceiba.pruebaceiba.R
import com.ceiba.pruebaceiba.adaptes.PublicaionesAdapter
import com.ceiba.pruebaceiba.data.Publicaciones
import com.ceiba.pruebaceiba.data.User
import com.ceiba.pruebaceiba.interfaces.APIService
import com.ceiba.pruebaceiba.mainModule.model.viewModel.MainViewModel
import com.ceiba.pruebaceiba.mainModule.viewModel.PublicacionesViewModel
import com.ceiba.pruebaceiba.services.ServicioUsuarios
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PublicacionesActivity : AppCompatActivity() {


    val adapterPublicaiones: PublicaionesAdapter = PublicaionesAdapter()
    lateinit var mRecyclerView : RecyclerView
    var publicaciones: MutableList<Publicaciones>  = ArrayList()
    private lateinit var nameView : TextView
    private lateinit var phoneView : TextView
    private lateinit var emailView : TextView
    private val progressDialog = CustomProgressDialog()
    private lateinit var publicacionesViewModel : PublicacionesViewModel
    var id = "0"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_publicaciones)

        initComponentes()
        initState()
        parseJson()
    }


    override fun onStart() {
        super.onStart()
        publicaciones.clear()
        setUpData()
    }

    private fun setUpData(){
        lifecycleScope.launch {
            publicacionesViewModel.getListaPublicaciones(id.toInt())
        }
    }



    fun initComponentes(){

        mRecyclerView = findViewById(R.id.rvPublicaciones) as RecyclerView
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(this);
        adapterPublicaiones.PublicaionesAdapter(publicaciones , this );
        mRecyclerView.adapter = adapterPublicaiones

        nameView = findViewById<TextView>(R.id.name)
        phoneView = findViewById<TextView>(R.id.phone)
        emailView = findViewById<TextView>(R.id.email)
    }

    fun initState (){
        val bundle:Bundle = intent.extras!!
        id = bundle.get("id") as String
        val name = bundle.get("name")
        val phone = bundle.get("phone")
        val email = bundle.get("email")

        nameView.text  = name.toString()
        phoneView.text = phone.toString()
        emailView.text = email.toString()



    }

    fun parseJson() {
        progressDialog.show(this)
        publicacionesViewModel = ViewModelProvider(this)[PublicacionesViewModel::class.java]
        publicacionesViewModel.getResult().observe(this){ result ->
            setUpdateUi(result)
        }
    }

    private fun setUpdateUi (data: Publicaciones
    ){
        publicaciones.add(data)
        progressDialog.dialog.dismiss()
        adapterPublicaiones.notifyDataSetChanged()
    }
}