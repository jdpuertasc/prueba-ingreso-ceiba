package com.ceiba.pruebaceiba.mainModule.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ceiba.pruebaceiba.data.Publicaciones
import com.ceiba.pruebaceiba.data.User
import com.ceiba.pruebaceiba.interfaces.APIService
import com.ceiba.pruebaceiba.mainModule.model.MainRespository
import com.ceiba.pruebaceiba.mainModule.model.PublicacionesRespository
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class PublicacionesViewModel : ViewModel() {

    private val result = MutableLiveData<Publicaciones>()

    fun getResult() : LiveData<Publicaciones> = result

    private val repository : PublicacionesRespository by lazy {
        val retrofit =  Retrofit.Builder()
            .baseUrl("https://jsonplaceholder.typicode.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val service = retrofit.create(APIService::class.java)
        PublicacionesRespository(service)

    }

    suspend fun getListaPublicaciones (id : Int) {

        viewModelScope.launch{
            val resultServer = repository.getPublicacioneLista(id )
            val resultado = resultServer.body()

            if(resultServer.isSuccessful){

                if (resultado != null) {
                    for (i in 0 until  resultado.count()){
                        val usuario = resultado[i]
                        result.value = usuario
                    }
                }
            }
        }
    }

}