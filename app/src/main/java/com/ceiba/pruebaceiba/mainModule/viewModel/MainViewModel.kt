package com.ceiba.pruebaceiba.mainModule.model.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ceiba.pruebaceiba.data.User
import com.ceiba.pruebaceiba.interfaces.APIService
import com.ceiba.pruebaceiba.mainModule.model.MainRespository
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    
    private val result = MutableLiveData<User>()

    fun getResult() : LiveData<User> = result

    private val repository : MainRespository by lazy {
        val retrofit =  Retrofit.Builder()
            .baseUrl("https://jsonplaceholder.typicode.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val service = retrofit.create(APIService::class.java)
        MainRespository(service)

    }

    suspend fun getListaUsuarios () {

        viewModelScope.launch{
            val resultServer = repository.getUsuariosLista()
            val resultado = resultServer.body()

                if(resultServer.isSuccessful){

                    if (resultado != null) {
                        for (i in 0 until  resultado.count()){
                            val usuario = resultado[i]
                            result.value = usuario
                        }
                    }
                }
            }
        }
    }

