package com.ceiba.pruebaceiba.mainModule.model

import com.ceiba.pruebaceiba.data.User
import com.ceiba.pruebaceiba.interfaces.APIService
import retrofit2.Response

class MainRespository (private val service: APIService){

    suspend fun getUsuariosLista () : Response<List<User>> {
        return service.getUsuarios()
    }

}