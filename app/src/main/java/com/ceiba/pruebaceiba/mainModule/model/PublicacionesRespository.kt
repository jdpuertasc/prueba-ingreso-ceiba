package com.ceiba.pruebaceiba.mainModule.model

import com.ceiba.pruebaceiba.data.Publicaciones
import com.ceiba.pruebaceiba.data.User
import com.ceiba.pruebaceiba.interfaces.APIService
import retrofit2.Response

class PublicacionesRespository  (private val service: APIService){

    suspend fun getPublicacioneLista (userId : Int) : Response<List<Publicaciones>> {
        return service.getPost( "posts?userId="+ userId.toString())
    }
}