package com.ceiba.pruebaceiba.services

import com.ceiba.pruebaceiba.data.User
import com.ceiba.pruebaceiba.interfaces.APIService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ServicioUsuarios {

    fun getRetrofit() : Retrofit {

        return  Retrofit.Builder()
            .baseUrl("https://jsonplaceholder.typicode.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

}