package com.ceiba.pruebaceiba.data

import com.google.gson.annotations.SerializedName

data class Address (
       val street : String,
       val suite : String,
       val city : String,
       val zipCode : String,
       val geo:  Geo)