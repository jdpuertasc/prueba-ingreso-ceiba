package com.ceiba.pruebaceiba.data

data class User(
      var id: String,
      var name: String,
      var username: String,
      var email: String,
      var phone: String,
      var website: String,
      val address: Address?,
      val company: Company?
) {



}






