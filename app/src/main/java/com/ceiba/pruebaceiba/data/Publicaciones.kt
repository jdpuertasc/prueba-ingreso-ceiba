package com.ceiba.pruebaceiba.data

import com.google.gson.annotations.SerializedName

data class Publicaciones (
     var userId: String,
     var id: String,
     var title: String,
     var body: String
        )