package com.ceiba.pruebaceiba.mainModule.viewModel

import com.ceiba.pruebaceiba.interfaces.APIService
import com.ceiba.pruebaceiba.mainModule.model.viewModel.MainViewModel
import kotlinx.coroutines.runBlocking
import org.hamcrest.MatcherAssert
import org.hamcrest.Matchers
import org.junit.Assert.*
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class PublicacionesViewModelTest {

    private lateinit var publicacionesViewModel : MainViewModel
    private  lateinit var services : APIService

    companion object{
        private lateinit var retrofit: Retrofit

        @BeforeClass
        @JvmStatic
        fun setupCommon(){
            retrofit = Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
    }

    @Before
    fun setUp(){
        publicacionesViewModel = MainViewModel()
        services = retrofit.create(APIService::class.java)
    }

    @Test
    fun checkCurrenUsuariosNotNullTest(){
        runBlocking{
            val result = services.getPost("posts?userId=1")
            MatcherAssert.assertThat(result.body(), Matchers.`is`(Matchers.notNullValue()))
        }
    }


    @Test
    fun checkNameUser(){

        runBlocking{
            val result = services.getPost("posts?userId=1")
            MatcherAssert.assertThat(result.body()!![0].title  ,
                Matchers.`is`("sunt aut facere repellat provident occaecati excepturi optio reprehenderit"))
        }

    }



}