package com.ceiba.pruebaceiba.mainModule.viewModel

import com.ceiba.pruebaceiba.interfaces.APIService
import com.ceiba.pruebaceiba.mainModule.model.viewModel.MainViewModel
import kotlinx.coroutines.runBlocking
import org.hamcrest.MatcherAssert.assertThat

import org.hamcrest.Matchers.`is`

import org.hamcrest.Matchers.notNullValue

import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class MainViewModelTest {

    private lateinit var mainViewModel : MainViewModel
    private  lateinit var services : APIService

    companion object{
        private lateinit var retrofit: Retrofit

        @BeforeClass
        @JvmStatic
        fun setupCommon(){
            retrofit = Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
    }

    @Before
    fun setUp(){
        mainViewModel = MainViewModel()
        services = retrofit.create(APIService::class.java)
    }

    @Test
    fun checkCurrenUsuariosNotNullTest(){
        runBlocking{
            val result = services.getUsuarios()
            assertThat(result.body(), `is`(notNullValue()))
        }
    }

    @Test
    fun checkNameUser(){

        runBlocking{
            val result = services.getUsuarios()
            assertThat(result.body()!![0].name  , `is`("Leanne Graham"))
        }

    }




}